package com.pillopl.acc.domain;

import io.vavr.API;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.Predicates.instanceOf;
import static io.vavr.collection.List.ofAll;

public class VirtualCreditCard {

    @Getter private final String refNo;
    private Limit limit;
    private int withdrawals;
    @Getter private List<DomainEvent> pendingEvents = new ArrayList<>();

    public VirtualCreditCard(String ref) {
        this.refNo = ref;
    }

    public VirtualCreditCard(String refNo, BigDecimal limit, BigDecimal usedLimit, int withdrawals) {
        this.refNo = refNo;
        this.limit = Limit.withUsed(limit, usedLimit);
        this.withdrawals = withdrawals;
    }

    public static VirtualCreditCard recreate(String ref, List<DomainEvent> events) {
        return ofAll(events).foldLeft(new VirtualCreditCard(ref), VirtualCreditCard::handleNext);
    }

    private VirtualCreditCard handleNext(DomainEvent domainEvent) {
        return API.Match(domainEvent).of(
                Case($(instanceOf(LimitAssigned.class)), this::limitAssigned),
                Case($(instanceOf(CardRepaid.class)), this::cardRepaid),
                Case($(instanceOf(CardWithdrawn.class)), this::cardWithdrawn),
                Case($(instanceOf(CycleClosed.class)), this::billingCycleClosed)

        );
    }


    public void assignLimit(BigDecimal amount) { //niebieskie
        limitAssigned(new LimitAssigned(refNo, amount, Instant.now()));
    }

    private VirtualCreditCard limitAssigned(LimitAssigned event) {
        this.limit = Limit.of(event.getAmount()); //pomaraczona
        pendingEvents.add(event);
        return this;
    }


    public void withdraw(BigDecimal amount) { //niebieska
        if (notEnoughMoneyToWithdraw(amount)) { //zolte
            throw new IllegalStateException();
        }
        if (tooManyWithdrawalsInCycle()) { //zolte
            throw new IllegalStateException();
        }
        //ack

        cardWithdrawn(new CardWithdrawn(refNo, amount, Instant.now()));

    }

    private VirtualCreditCard cardWithdrawn(CardWithdrawn event) {
        this.limit = limit.subtract(event.getAmount());
        this.withdrawals++;
        pendingEvents.add(event);
        return this;

    }

    private boolean tooManyWithdrawalsInCycle() {
        return withdrawals >= 45;
    }

    private boolean notEnoughMoneyToWithdraw(BigDecimal amount) {
        return availableLimit().compareTo(amount) < 0;
    }

    public BigDecimal availableLimit() {
        return limit.available();
    }

    public void repay(BigDecimal amount) {
        cardRepaid(new CardRepaid(refNo, amount, Instant.now()));

    }

    private VirtualCreditCard cardRepaid(CardRepaid event) {
        this.limit = limit.add(event.getAmount());
        pendingEvents.add(event);
        return this;

    }

    public void cycleClosed() {
        billingCycleClosed(new CycleClosed(refNo, Instant.now()));
    }

    private VirtualCreditCard billingCycleClosed(CycleClosed event) {
        this.withdrawals = 0;
        pendingEvents.add(event);
        return this;

    }

    public void flushEvents() {
        pendingEvents.clear();
    }

}
