package com.pillopl.acc.domain;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class VirtualCreditCardJUnitTest {


    /**
     * Make this test pass by implementing VirtualCreditCard.recreate
     * Come up with new scenarios of recreation (different set of events).
     */
    @Test
    public void shouldRecreate() {
        //given
        List<DomainEvent> events = new ArrayList<>();
        events.add(new LimitAssigned("ref", new BigDecimal(100.00), Instant.now()));
        events.add(new CardWithdrawn("ref", new BigDecimal(10.00), Instant.now()));
        events.add( new CardRepaid("ref", new BigDecimal(5.00), Instant.now()));

        //when
        VirtualCreditCard creditCard = VirtualCreditCard.recreate("ref", events);

        //then
        assertThat(creditCard.availableLimit()).isEqualByComparingTo(new BigDecimal(95.00));


    }

}